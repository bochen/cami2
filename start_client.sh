LOG=hash_client_$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 32).log
python src/main/hash_client.py -i NonEukaryota -a localhost --port 6379 \
--hash input/lsh/lsh_CAMI2_illumina_k15_h25.crp --type illumina --max_num_msg 1000000 \
> $LOG 2>&1 &

