#!/bin/bash

#change this if you think TOTAL_CORES is not correct
export TOTAL_CORES=`grep -c ^processor /proc/cpuinfo`
echo "The node has $TOTAL_CORES cores"
export LGB_CORES=16

export CAMI2_HOME="$( cd "$(dirname "$0")" ; pwd -P )"
echo use home: $CAMI2_HOME

export PYTHONPATH=$CAMI2_HOME/src/main

source activate python27

cd $CAMI2_HOME/src/main
python step1_make_data.py 			|| exit -1
python step2_make_hash_function.py 			|| exit -1
python sparc_step10_make_query_db.py 			|| exit -1


source deactivate python27 

