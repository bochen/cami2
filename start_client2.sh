LOG=hash_client_$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 32).log
./hash_client --db /mnt/cami2/input/db/nt_split_NonEukaryota.db --redis tcp://127.0.0.1:6379 --queue QUEUE  \
--min 1000000 --hash /mnt/cami2/input/lsh/lsh_CAMI2_illumina_k15_h25.crp --type illumina --tax input/nt_info_NonEukaryota_taxidmap.csv \
> $LOG 2>&1 &

