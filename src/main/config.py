
import os
import utils
import platform

if 'CAMI2_HOME' not in os.environ:
    HOME = os.path.join(os.environ['HOME'], 'cami2')
else:
    HOME = os.environ['CAMI2_HOME']

assert HOME is not None 

HOSTNAME = platform.node()

IS_BRIDGES = "bridges" in HOSTNAME or ("SLURM_CLUSTER_NAME" in os.environ and "bridges" in os.environ["SLURM_CLUSTER_NAME"])
if IS_BRIDGES:
    assert ("SLURM_CLUSTER_NAME" in os.environ and "bridges" in os.environ["SLURM_CLUSTER_NAME"])

INPUT_PATH = os.path.join(HOME, "input")
TASK_PATH = os.path.join(INPUT_PATH, 'task')
INFO_PATH = os.path.join(INPUT_PATH, 'info')
SEQ_PATH = os.path.join(INPUT_PATH, 'seqs')
SPARC_PATH = os.path.join(INPUT_PATH, 'sparc')
LSH_PATH = os.path.join(INPUT_PATH, 'lsh')
DB_PATH = os.path.join(INPUT_PATH, 'db')

[utils.create_dir_if_not_exists(directory) for directory in [INPUT_PATH, TASK_PATH, INFO_PATH, SEQ_PATH, SPARC_PATH, LSH_PATH, DB_PATH]]

if IS_BRIDGES:
    LOCAL_PATH = os.environ['LOCAL']
    MEMDISK_PATH = os.environ['RAMDISK']
    assert os.path.exists(LOCAL_PATH)
    assert os.path.exists(MEMDISK_PATH)
else:
    LOCAL_PATH = os.path.join(INPUT_PATH, 'local')
    MEMDISK_PATH = os.path.join(INPUT_PATH, 'memdisk')
    [utils.create_dir_if_not_exists(directory) for directory in [LOCAL_PATH, MEMDISK_PATH]]
    
NR_ZIP = os.path.join(INPUT_PATH, 'nr.gz')
NT_ZIP = os.path.join(INPUT_PATH, 'nt.gz')


def get_java_memory(n_thread):
    if IS_BRIDGES:
        mem = n_thread * 30
    else:
        from psutil import virtual_memory
        mem = virtual_memory()
        mem = int(mem.total * 0.8 / 1024 ** 3)    
    return mem

