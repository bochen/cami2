'''
Created on Feb 23, 2019

@author:  Lizhen Shi
'''

import numpy as np
import config, utils
import os
import cPickle as pickle
import pandas as pd 


class NCBITaxonomy:

    def __init__(self):
        self.logger = utils.get_logger("NCBITaxonomy")
        self.taxid_to_ids = pd.read_csv(os.path.join(config.INFO_PATH, 'taxid_to_ids.csv.gz'), index_col=0, sep='\t')
        # self.logger.info("\n" + str(self.taxid_to_ids.head()))
        # self.logger.info("\n" + str(self.taxid_to_ids.dtypes))
        with open(os.path.join(config.INFO_PATH, 'merged_tax_map.pkl'), 'rb') as fin:
            self.merged_ids = pickle.load(fin)
        self.m_species_ids = None

    def taxid_to_species_id(self, taxid):
        if self.m_species_ids is None:
            self.m_species_ids = self.taxid_to_ids['species'].to_dict()
        if taxid in self.merged_ids:
            taxid = self.merged_ids[taxid]
        return self.m_species_ids[taxid]

    def make_map_fun(self, labeltype):
        if labeltype == 'species':
            return self.taxid_to_species_id
        else:
            raise Exception("unknown " + labeltype)
