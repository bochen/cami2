#!/usr/bin/env python

import os
from hashlib import sha256
import sys, getopt
import utils
import config
from joblib import Parallel, delayed
from myzstd import ZstdFile
import data
import pandas as pd 
from task import Task
    
logger = utils.get_logger("train_jclf")


def make_embedding(model_file, hash_file, n_thread, tmp_dir):
    logger.info("make_embedding ... ")
    
    model_file = get_local(model_file, tmp_dir)
    hash_file = get_local(hash_file, tmp_dir)
    
    files = utils.list_dir(tmp_dir, "split_seq_*")
    logger.info("has {} files".format(len(files)))
    
    cmd = "ls split_seq_* | xargs -L1 -P{thread} -IINPUT java -Xmx32G -jar {jar} embed --inputModel {model} --input INPUT --output embedding_INPUT --hash {hash}".format(
        jar=os.path.join(config.HOME, "lib", "jfastseq-*-*.jar"), hash=hash_file, model=model_file , thread=n_thread)
    
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir, logger=logger)
    if status != 0:
        logger.info("run jfastseq failed")
        raise Exception("run jfastseq failed")
    else:
        logger.info("finish running train")


def copy_input(seq_file, model_file, hash_file, tmp_dir):
    if not config.IS_BRIDGES: 
        return 
    for in_file in [hash_file, seq_file, model_file]:
        inbase = os.path.basename(in_file);
        stage = os.path.join(tmp_dir, inbase);    
        logger.info("Copying file from {} to {}".format(in_file, stage))
        
        cmd = """
        RC=1
        n=0
        while [[ $RC -ne 0 && $n -lt 20 ]]; do
            rsync -aP {sourcedir} {tmp_dir}
            RC=$?
            let "n = n + 1"
            sleep 10
        done
    
        """.format(sourcedir=in_file, tmp_dir=tmp_dir)
        
        status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir, logger=logger)
        if status != 0:
            logger.error("Run command failed for: " + cmd)
            raise Exception("failed")

        
def copy_output(out_file, tmp_dir):
    cmd = "cat embedding_split_seq_* "
    if out_file.endswith('.gz'):
        cmd +="| gzip "
    elif out_file.endswith('.zst'):
        cmd +="| zstd "
    cmd +="> "+out_file
                
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir, logger=logger)
    if status != 0:
        logger.error("Run command failed for: " + cmd)
        raise Exception("failed")

def get_local(fpath, tmp_dir):
    inbase = os.path.basename(fpath);
    localfile = os.path.join(tmp_dir, inbase);
    return localfile


def split_input(seq_file, tmp_dir):
    localfile = get_local(seq_file, tmp_dir)
    cmd = "cat {} ".format(localfile)
    if localfile.endswith(".gz"):
        cmd += "| gunzip "
    elif localfile.endswith(".zst"):
        cmd += "| zstd -d "
    
    cmd += "| split -C 500M - split_seq_"
        
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir, logger=logger)
    if status != 0:
        logger.error("Run command failed for: " + cmd)
        raise Exception("failed")    


def preparation(tmp_dir):
    utils.remove_if_file_exit(tmp_dir, is_dir=True)
    utils.create_dir_if_not_exists(tmp_dir)

                
def run(in_file, model_file, hash_file, out_file, tmp_dir, n_thread):
    params = dict(locals())
    logger.info("params: " + str(params))
    thisid = sha256(str(params)).hexdigest();
    if tmp_dir is None: 
        tmp_dir = os.path.join(config.MEMDISK_PATH, thisid)
    logger.info("use tmp dir: " + tmp_dir)
    logger.info("n_thread: " + str(n_thread))

    if 1:
        task = Task('make_embeding-preparation-{}'.format(thisid), lambda _: preparation(tmp_dir))
        task()
    if 1:
        task = Task('make_embeding-copy_input-{}'.format(thisid), lambda _: copy_input(in_file, model_file, hash_file, tmp_dir))
        task()
    if 1:
        task = Task('make_embeding-split_seqfile-{}'.format(thisid), lambda _: split_input(in_file, tmp_dir))
        task()
                        
    if 1:
        task = Task('make_embedding-embeding-{}'.format(thisid), lambda _: make_embedding(model_file, hash_file, n_thread, tmp_dir))
        task()        

    if 1:
        task = Task('make_embedding-copy_output-{}'.format(thisid), lambda _: copy_output(out_file, tmp_dir))
        task()        
        
def main(argv):
    in_file = ''
    out_file = ''
    model_file = ""
    hash_file = ""
    n_thread = utils.get_num_thread()
    tmp_dir = None
    help_msg = sys.argv[0] + ' -i <seq file>  -o <output file> --model <model file> --hash <hash file>  [--thread <num thread>  --tmpdir <tmp dir>]'    
    if len(argv) < 2:
        print (help_msg)
        sys.exit(2) 
    else:
        try:
            opts, args = getopt.getopt(argv, "hi:o:", ["model=", "thread=", 'tmpdir=', 'hash='])
        except getopt.GetoptError:
            print (help_msg)
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print (help_msg)
                sys.exit()
            elif opt in ("-i", "--in_file"):
                in_file = arg
            elif opt in ("--thread"):
                n_thread = int(arg) 
                assert n_thread > 0
            elif opt in ("--tmpdir"):
                tmp_dir = arg
            elif opt in ("--model"):
                model_file = arg
            elif opt in ("--hash"):
                hash_file = arg                                    
            elif opt in ("-o", "--out_file"):
                out_file = arg

    excepttext = []
    
    if not utils.file_exists(in_file):
        excepttext.append(in_file + " not exists")
    if not utils.file_exists(model_file):
        excepttext.append(model_file + " not exists")        
    if not utils.file_exists(hash_file):
        excepttext.append(hash_file + " not exists")        
    in_file = os.path.abspath(in_file)
    out_file = os.path.abspath(out_file)
    model_file = os.path.abspath(model_file)
    hash_file = os.path.abspath(hash_file)
    
    if excepttext:
        raise Exception(str(excepttext))
    
    run(in_file, model_file, hash_file, out_file, tmp_dir, n_thread);


if __name__ == "__main__":
    main(sys.argv[1:])
