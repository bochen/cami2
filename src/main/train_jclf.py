#!/usr/bin/env python

import os
from hashlib import sha256
import sys, getopt
import utils
import config
from joblib import Parallel, delayed
from tqdm import tqdm
from myzstd import ZstdFile
import data
import pandas as pd 
from task import Task
    
logger = utils.get_logger("train_jclf")


def train(in_folder, tmp_dir, outmodel, params, n_thread):
    logger.info("training model ... ")
    params['output'] = outmodel
    params['tree'] = os.path.join(config.INFO_PATH, 'ncbi_tree.json.gz')
    cmd = "java -server -Xmx{} -XX:+AggressiveOpts -cp {} net.jfastseq.Main train ".format(
        utils.get_java_memory(n_thread),
        os.path.join(config.HOME, "lib", "jfastseq-*-*.jar"))
    # cmd += "train --input data/sample_hashed_?? --output data/model --tree data/ncbi_tree.json.gz --lr 0.4 --epoch 3 --dim 50 --thread 2 "
    cmd += " ".join('--{} {}'.format(u, v) for u, v in params.items())
    cmd += " --thread {} ".format(n_thread)
    inbase = os.path.basename(in_folder);
    # stage = os.path.join(tmp_dir, inbase);
    stage = inbase    

    cmd += " --input {}/reads_*.hashed*".format(stage)
        
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir, logger=logger)
    if status != 0:
        logger.info("run jfastseq failed")
        raise Exception("run jfastseq failed")
    else:
        logger.info("finish running train")


def copy_input(in_file, tmp_dir):
    if not config.IS_BRIDGES: 
        return in_file
    inbase = os.path.basename(in_file);
    stage = os.path.join(tmp_dir, inbase);    
    logger.info("Copying file from {} to {}".format(in_file, stage))
    
    cmd = """
    RC=1
    n=0
    while [[ $RC -ne 0 && $n -lt 20 ]]; do
        rsync -aP {sourcedir} {tmp_dir}
        RC=$?
        let "n = n + 1"
        sleep 10
    done

    """.format(sourcedir=in_file, tmp_dir=tmp_dir)
    
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir, logger=logger)
    if status != 0:
        logger.error("Run command failed for: " + cmd)
        raise Exception("failed")    

    return stage


def preparation(in_file, tmp_dir):
    utils.remove_if_file_exit(tmp_dir, is_dir=True)
    utils.create_dir_if_not_exists(tmp_dir)

                
def run(in_file, out_file, tmp_dir, fastseq_args, n_thread):
    params = dict(locals())
    params['alg'] = 'jfastseq'
    logger.info("params: " + str(params))
    thisid = sha256(str(params)).hexdigest();
    if tmp_dir is None: 
        tmp_dir = os.path.join(config.LOCAL_PATH, thisid)
    logger.info("use tmp dir: " + tmp_dir)
    logger.info("n_thread: " + str(n_thread))

    if 1:
        task = Task('train_jclf-preparation-{}'.format(thisid), lambda _: preparation(in_file, tmp_dir))
        task()
    if 1:
        task = Task('train_jclf-copy_input-{}'.format(thisid), lambda _: copy_input(in_file, tmp_dir))
        task()
                
    if 1:
        train(in_file, tmp_dir, out_file, fastseq_args, n_thread)

        
def main(argv):
    in_file = ''
    out_file = ''
    lr = 0.1
    epoch = 5
    dim = 100
    n_thread = utils.get_num_thread()
    tmp_dir = None
    help_msg = sys.argv[0] + ' -i <hashed file folder>  -o <output model>   [--lr <lr> --ws <ws> --epoch <epoch> --inputModel <input model> --bucket <bucket> --dim <dim> --thread <num thread>  --tmpdir <tmp dir>]'    
    if len(argv) < 2:
        print (help_msg)
        sys.exit(2) 
    else:
        try:
            opts, args = getopt.getopt(argv, "hi:o:", ["thread=", 'tmpdir=', 'hash=', 'lr=', 'epoch=', 'dim='])
        except getopt.GetoptError:
            print (help_msg)
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print (help_msg)
                sys.exit()
            elif opt in ("-i", "--in_file"):
                in_file = arg
            elif opt in ("--thread"):
                n_thread = int(arg) 
                assert n_thread > 0
            elif opt in ("--tmpdir"):
                tmp_dir = arg
            elif opt in ("-o", "--out_file"):
                out_file = arg
            elif opt in ("--lr"):
                lr = float(arg)
                assert lr > 0 
            elif opt in ("--epoch"):
                epoch = int(arg)
                assert epoch > 0
            elif opt in ("--dim"):
                dim = int(arg)
                assert dim > 0

    excepttext = []
    
    if False and "SPARK_HOME" not in os.environ:
        excepttext.append("must specify SPARK_HOME env")
    
    if not utils.file_exists(in_file):
        excepttext.append(in_file + " not exists")
    in_file = os.path.abspath(in_file)
    out_file = os.path.abspath(out_file)
    
    if excepttext:
        raise Exception(str(excepttext))
    
    fastseq_args = dict(lr=lr, epoch=epoch, dim=dim)
    run(in_file, out_file, tmp_dir, fastseq_args, n_thread);


if __name__ == "__main__":
    main(sys.argv[1:])
