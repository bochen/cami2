import fastparquet 
import pandas as pd 
import numpy as np
import config, utils
from task import Task
import os
import gzip
from joblib import Parallel, delayed
import cPickle as pickle
from rand_proj import RandProj


def make_hash(name, readpath, kmer_length, hash_length):

    def f(logger):
        reads = []
        with gzip.open(readpath) as fin:
            for line in fin:
                reads.append(line.strip())
        logger.info("load {} reads from {}".format(len(reads), readpath))
        
        outfilename = os.path.join(config.INPUT_PATH, "lsh_{}_k{}_h{}.pkl".format(name, kmer_length, hash_length))
        rp = RandProj(kmer_size=kmer_length, hash_size=hash_length, n_thread=utils.get_num_thread())
        rp.create_hash(reads)
        logger.info("write " + outfilename)
        pickle.dump(rp, open(outfilename, 'wb'))
        
    task = Task('make_hash-{}-k{}-h{}'.format(name, kmer_length, hash_length), lambda u: f(u))
    task()

               
def run():
    if 0:
        for name, readpath in zip(['CAMI2_illumina', 'CAMI2_pacbio', 'nt_NonEukaryota'],
                                 ("CAMI2_illumina_sample.seq.gz", "CAMI2_pacbio_sample.seq.gz", "nt_NonEukaryota_sample.seq.gz")):
            readpath = os.path.join(config.INPUT_PATH, readpath)
            for k in [15, 23, 31]:
                for h in [25, 31]:
                    make_hash(name, readpath, kmer_length=k, hash_length=h)


if __name__ == '__main__':
    run()

