#!/usr/bin/env python

import os
import sys, getopt
import utils
import config
from joblib import Parallel, delayed
from tqdm import tqdm
import pandas as pd  
from hashlib import sha256
from task import Task
import randproj
import gzip
import csparc
import cPickle as pickle    
logger = utils.get_logger("cami2_seq_predict")


def hash_line(line, kmer_size):
    _, readid, seq = line.strip().split("\t")
    kmers = csparc.generate_kmer_for_fastseq(seq, kmer_size)
    hashed = [randproj.hash_kmer(kmer, reverse_compliments=True) for kmer in kmers]
    hashed = [csparc.ulong2base64(i) for i in hashed]
    newline = "__label__" + readid + "\t" + " ".join(hashed) + "\n"
    return newline


def hash_a_file(args, seqfile):
    os.chdir(args.tmp_dir)

    lshfile = args.lshfile
    
    status = randproj.load_from_file(lshfile)
    assert status == 0, 'load hash failed for ' + lshfile
    kmer_size = randproj.get_kmer_size()
    
    hashedreadfile = seqfile + ".hashed"
    logger.info('hashing {} to {}, k={}'.format(seqfile, hashedreadfile, kmer_size))
    with (gzip.open(seqfile) if seqfile.endswith('.gz') else open(seqfile)) as fin:
        with open(hashedreadfile, 'w') as fout:
            for line in fin:
                newline = hash_line(line, kmer_size)
                fout.write(newline)
    return hashedreadfile

    
def hashfiles(args, n_thread):
    os.chdir(args.tmp_dir)
    seqfiles = args.splited_seq_files
    hashedfiles = Parallel(n_jobs=n_thread)(delayed(hash_a_file)(args, u) for u in tqdm(seqfiles))
    args.hashedfiles = hashedfiles
    

def preparation(args):
    tmp_dir = args.tmp_dir
    utils.remove_if_file_exit(tmp_dir, is_dir=True)
    utils.create_dir_if_not_exists(tmp_dir)


def copy_input(args):
    in_file = args.seq_file
    tmp_dir = args.tmp_dir
    if not config.IS_BRIDGES: 
        args.staged_input = in_file
        return in_file
    inbase = os.path.basename(in_file);
    staged_input = os.path.join(tmp_dir, inbase);
    args.staged_input = staged_input    
    logger.info("Copying file from {} to {}".format(in_file, staged_input))
    
    cmd = """
    RC=1
    n=0
    while [[ $RC -ne 0 && $n -lt 20 ]]; do
        rsync -aP {sourcedir} {tmp_dir}
        RC=$?
        let "n = n + 1"
        sleep 10
    done

    """.format(sourcedir=in_file, tmp_dir=tmp_dir)
    
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir)
    if status != 0:
        logger.error("Run command failed for: " + cmd)
        raise Exception("failed")    

    return staged_input


def copy_model(args):
    in_file = args.modelpath
    tmp_dir = args.tmp_dir
    if not config.IS_BRIDGES: 
        args.local_modelpath = in_file
        return in_file
    inbase = os.path.basename(in_file);
    staged_input = os.path.join(tmp_dir, inbase);
    args.local_modelpath = staged_input    
    logger.info("Copying file from {} to {}".format(in_file, staged_input))
    
    cmd = """
    RC=1
    n=0
    while [[ $RC -ne 0 && $n -lt 20 ]]; do
        rsync -aP {sourcedir} {tmp_dir}
        RC=$?
        let "n = n + 1"
        sleep 10
    done

    """.format(sourcedir=in_file, tmp_dir=tmp_dir)
    
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir)
    if status != 0:
        logger.error("Run command failed for: " + cmd)
        raise Exception("failed")    

    return staged_input


def predict_a_file(fname, local_modelpath, k, threshold, tmp_dir):
    outfile = fname + ".predict"
    logger.info("predict " + fname)
    
    cmd = "fastseq predict-prob {} {} {} {} | sed 's/__label__//g' > {} ".format (local_modelpath, fname, k, threshold, outfile)
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir)
    if status != 0:
        logger.error("Run command failed for: " + cmd)
        raise Exception("failed")    

    return outfile

    
def predict(args, n_thread):
    tmp_dir = args.tmp_dir
    
    os.chdir(tmp_dir)
    files = args.hashedfiles
    assert len(files) > 0
    
    predoutfiles = Parallel(n_jobs=n_thread)(delayed(predict_a_file)(u, args.local_modelpath, args.k, args.threshold, args.tmp_dir) for u in tqdm(files))
    args.predoutfiles = predoutfiles


def merge_predict(args):
    os.chdir(args.tmp_dir)
    files = args.hashedfiles
    assert len(files) > 0   
    tmpoutput = "tmpprediction.txt"
    with open(tmpoutput, 'wt') as fout: 
        for fname in files:
            readids = pd.read_csv(fname, header=None, sep="\t", usecols=[0]).iloc[:, 0].map(lambda u:u.replace("__label__","")).values
            with open(fname + ".predict") as fin:
                predictions = list(fin)
            assert len(readids) == len(predictions)
            for a, b in zip(readids, predictions):
                line = a + "\t" + b
                fout.write(line)
    args.tmpprediction = tmpoutput

    
def split_seqfile(args):
    seqfile = args.staged_input
    cmd = "cat {} | gunzip| split -C 100M - reads_split_ ".format(seqfile)
    status = utils.shell_run_and_wait2(cmd, args.tmp_dir, logger=logger)
    if status <> 0:
        raise Exception("failed")
    args.splited_seq_files = utils.list_dir(args.tmp_dir, 'reads_split_*')


def copy_result(args):
    cmd = "cat {} | zstd > {}".format(args.tmpprediction, args.out_file)
    status = utils.shell_run_and_wait2(cmd, args.tmp_dir, logger=logger)
    if status <> 0:
        raise Exception("failed")

    
class Args():

    def __init__(self):
        pass
        
    def init(self, seq_file, out_file, lshfile, modelpath, k, threshold, tmp_dir):
        self.seq_file, self.out_file, self.lshfile, self.k, self.threshold, self.tmp_dir = seq_file, out_file, lshfile, k, threshold, tmp_dir
        self.modelpath = modelpath
        
    def save(self, fname):
        with open(fname, 'wb') as fin:
            pickle.dump(self, fin);

        
def run(in_file, lshfile, predict_out, modelpath, k, threshold, tmp_dir, n_thread):
    params = str(locals())
    logger.info("params: " + params)
    thisid = sha256(params).hexdigest();
    if tmp_dir is None: 
        tmp_dir = os.path.join(config.MEMDISK_PATH, thisid)
    logger.info("use tmp dir: " + tmp_dir)
    logger.info("n_thread: " + str(n_thread))
    
    argpicklefile = tmp_dir + "/args.pkl"
    if utils.file_exists(argpicklefile):
        args = pickle.load(open(argpicklefile, 'rb'))
    else:
        args = Args()
    args.init(in_file, predict_out, lshfile, modelpath, k, threshold, tmp_dir) 
    
    if 1:
        task = Task('cami_predict-preparation-{}'.format(thisid), lambda _: preparation(args))
        task(); args.save(argpicklefile)
        task = Task('cami_predict-copy_input-{}'.format(thisid), lambda _: copy_input(args))
        task(); args.save(argpicklefile)
        task = Task('cami_predict-copy_model-{}'.format(thisid), lambda _: copy_model(args))
        task(); args.save(argpicklefile)        
    if 1:
        task = Task('cami_predict-split_hashed-{}'.format(thisid), lambda _: split_seqfile(args))
        task(); args.save(argpicklefile)

    if 1:
        task = Task('cami_predict-hashfile-{}'.format(thisid), lambda _: hashfiles(args, n_thread))
        task(); args.save(argpicklefile)
    if 1:
        task = Task('cami_predict-predict-{}'.format(thisid), lambda _: predict(args, n_thread))
        task(); args.save(argpicklefile)
    if 1:
        task = Task('cami_predict-merge_predict-{}'.format(thisid), lambda _: merge_predict(args))
        task(); args.save(argpicklefile)

    if 1:
        task = Task('cami_predict-copy_result-{}'.format(thisid), lambda _: copy_result(args))
        task(); args.save(argpicklefile)

                    
def main(argv):
    in_file = ''
    out_file = ''
    lshfile = ""
    threshold = 0.5 / 100
    k = 300
    n_thread = utils.get_num_thread()
    tmp_dir = None
    help_msg = sys.argv[0] + ' -i <seqfile>  -o <outputfile> --hash <hash_file> [--thread <num thread>  --tmpdir <tmp dir>]'
    if len(argv) < 2:
        print (help_msg)
        sys.exit(2) 
    else:
        try:
            opts, args = getopt.getopt(argv, "hi:o:k:", ["thread=", 'tmpdir=', 'hash=', 'model=', 'threshold='])
        except getopt.GetoptError:
            print (help_msg)
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print (help_msg)
                sys.exit()
            elif opt in ("-i", "--in_file"):
                in_file = arg
            elif opt in ("--thread"):
                n_thread = int(arg) 
                assert n_thread > 0
            elif opt in ("--tmpdir"):
                tmp_dir = arg
            elif opt in ("--hash"):
                lshfile = arg
            elif opt in ("--model"):
                modelpath = arg                                      
            elif opt in ("-k"):
                k = int(arg)     
            elif opt in ("--threshold"):
                threshold = float(arg)                                                     
            elif opt in ("-o", "--out_file"):
                out_file = arg  

    assert "CAMI2_HOME" in os.environ
    assert utils.file_exists(in_file), in_file
    in_file = os.path.abspath(in_file)
    
    assert utils.file_exists(lshfile), lshfile
    lshfile = os.path.abspath(lshfile)

    assert utils.file_exists(modelpath), modelpath
    modelpath = os.path.abspath(modelpath)

    out_file = os.path.abspath(out_file)
    if utils.file_exists(out_file) :
        raise Exception("Delete output first: " + out_file)
    
    run(in_file, lshfile, out_file, modelpath, k, threshold, tmp_dir, n_thread);


if __name__ == "__main__":
    main(sys.argv[1:])

