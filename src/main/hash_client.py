#!/usr/bin/env python

import os
import sys, getopt
import re
import utils
import config
import randproj
import csparc
import redis
import fastparquet
from read_pb2 import Read
import time
import cPickle as pickle
import numpy as np
                             
logger = utils.get_logger("hash_client")


def get_NonEukaryota():
    fname = os.path.join(config.INPUT_PATH, 'nt_info_NonEukaryota_taxidmap.pkl')
    if utils.file_exists(fname):
        logger.info("reading " + fname)
        with open(fname, 'rb') as fin:
            taxmap = pickle.load(fin)
            logger.info("length of dict = " + str(len(taxmap)))
            return taxmap
    else:
        logger.info("making " + fname)
        df2 = fastparquet.ParquetFile(os.path.join(config.INPUT_PATH, 'nt_info_NonEukaryota.parq')).to_pandas()
        logger.info("df2.shape=" + str(df2.shape))
        df3 = df2[df2['superkingdom'].isin({'Bacteria', 'Viruses', 'Archaea', 'Viroids'})]
    
        nas = set(['NA', 'NA2'])
        df10 = df3[(~df3['superkingdom'].isin(nas))]
        df10=df3
        logger.info("total length {}/{}".format(df10['length'].sum(), df2['length'].sum()))
        df10[['row','taxid','length']].to_csv(os.path.join(config.INPUT_PATH, 'nt_info_NonEukaryota_taxidmap.csv'), header=None,index=None,sep=" ")
        df11 = df10.set_index('row')['taxid']
        taxmap = df11.to_dict()
        logger.info("length of dict = " + str(len(taxmap)))
        with open(fname, 'wb') as fout:
            pickle.dump(taxmap, fout)
        return taxmap 

            
class HashClient():

    def __init__(self, hash_file, in_db, address, port, max_num_msg, read_type):
        logger.info("params: " + str(locals()))        
        self.hash_file, self.in_db, self.address, self.port, self.max_num_msg, self.read_type = hash_file, in_db, address, port, max_num_msg , read_type
        self.db = None 
        self.conn = None
        self.keysize = 0
        self.KEY = "QUEUE"

    def init(self):
        self.load_hash()
        self.initSim()
        self.connect()
        self.open_db()

    def initSim(self):
        if self.read_type == "illumina":
            import art_illumina as cart 
            cart.initialize("-l 150  -m 270 -s 27 -f 1")
            self.sim = cart 
            self.min_ref_len = 400
        else:
            import pbsim  
            pbsim.initialize("--data-type CLR  --depth 1 --length-mean 3000 --length-sd 1000 --model_qc {}/data/model_qc_clr".format(config.HOME));
            self.sim = pbsim
            self.min_ref_len = 6000
        
    def open_db(self):
        if self.in_db == "NonEukaryota":
            self.candidate_map = get_NonEukaryota()
            self.dbpath = os.path.join(config.DB_PATH, "nt_split_NonEukaryota.db")
        else:
            raise Exception("Unsupported " + self.in_db)        
        self.db = utils.create_db(self.dbpath)
    
    def load_hash(self):
        hash_file = self.hash_file
        status = randproj.load_from_file(hash_file)
        assert status == 0, 'load hash failed for ' + hash_file
        self.kmer_size = randproj.get_kmer_size()
        
    def connect(self):
        self.conn = redis.Redis(
            host=self.address,
            port=self.port)

    def poll_queue_size(self):
        self.keysize = self.conn.scard(self.KEY)
        logger.info("Queue Size = "+str(self.keysize))        
        
    def write_db1(self, lst):
        with self.conn.pipeline() as pipe:
            for taxid, hashed in lst:
                read = Read()
                read.label = taxid
                read.kmer.extend(hashed)
                pipe.sadd(self.KEY, read.SerializeToString())

    def write_db(self, lst):
        values = []
        for taxid, hashed in lst:
            read = Read()
            read.label = taxid
            read.kmer.extend(hashed)
            values.append(read.SerializeToString())
        self.conn.sadd(self.KEY, *values)
                
    def run1(self):
        sub_re = re.compile(r"[^ACGT]")
        db = self.db
        candidate_map = self.candidate_map
        it = db.iteritems()
        it.seek_to_first()
        n_keys = 0
        n_reads = 0         
        batch = []
        last_poll_time = 0
        while(True):
            curr_time = time.time()
            if curr_time - last_poll_time > 5:
                self.poll_queue_size()
                last_poll_time = curr_time
                
            if self.keysize < self.max_num_msg:
                if 1:
                    key, val = next(it)
                    if len(val) < self.min_ref_len: continue
                    
                    if n_keys % 1000 == 0:
                        logger.info("created {} reads from {} refs".format(n_reads, n_keys))
                    
                    n_keys += 1
                    if 0 and (n_keys % 100000 == 0):
                        print("db iteration: " + str(n_keys))
                    key = int(key)
                    if key in candidate_map:
                        refseq = sub_re.sub("N", val.strip())
                        taxid = candidate_map[key]
                        reads = self.sim.simulate(refseq)
                        for read in reads:
                            kmers = csparc.generate_kmer_for_fastseq(read, self.kmer_size)
                            batch.append((taxid, [randproj.hash_kmer(kmer, reverse_compliments=True) for kmer in kmers]))
                            if len(batch) >= 100:
                                self.write_db(batch)
                                n_reads += len(batch)
                                batch = []
            else:
                time.sleep(2)
    def run(self):
        sub_re = re.compile(r"[^ACGT]")
        db = self.db
        candidate_map = self.candidate_map
        n_keys = 0
        n_reads = 0         
        batch = []
        last_poll_time = 0
        keys = list(candidate_map.keys())
        np.random.shuffle(keys);
        while(True):
            for key in keys:
                curr_time = time.time()
                if curr_time - last_poll_time > 5:
                    self.poll_queue_size()
                    last_poll_time = curr_time
                    
                if self.keysize < self.max_num_msg:
                    if 1:
                        val=db.get(str(key))
                        if len(val) < self.min_ref_len: continue
                        
                        if n_keys % 1000 == 0:
                            logger.info("created {} reads from {} refs".format(n_reads, n_keys))
                        
                        n_keys += 1
                        if 0 and (n_keys % 100000 == 0):
                            print("db iteration: " + str(n_keys))
                        key = int(key)
                        if key in candidate_map:
                            refseq = sub_re.sub("N", val.strip())
                            taxid = candidate_map[key]
                            reads = self.sim.simulate(refseq)
                            for read in reads:
                                kmers = csparc.generate_kmer_for_fastseq(read, self.kmer_size)
                                batch.append((taxid, [randproj.hash_kmer(kmer, reverse_compliments=True) for kmer in kmers]))
                                if len(batch) >= 100:
                                    self.write_db(batch)
                                    n_reads += len(batch)
                                    batch = []
                else:
                    time.sleep(1)

    def close(self):
        if self.db is not None:
            # self.db.close()
            pass 
            
        if self.conn is not None:
            # self.conn.close() 
            pass                  


def main(argv):
    in_db = ''
    address = "127.0.0.1"
    hash_file = ""
    port = 6379
    max_num_msg = 1000 
    read_type = "illumina"
    
    help_msg = sys.argv[0] + ' -i <rockdb>  -a <redis address> -p <port> --hash <hash file> --type <illumina or pacbio> [--max_num_msg <max_num_msg>]'
    if len(argv) < 2:
        print (help_msg)
        sys.exit(2) 
    else:
        try:
            opts, args = getopt.getopt(argv, "hi:a:", ["max_num_msg=", 'hash=', 'db=', "address=", "port=", "type="])
        except getopt.GetoptError:
            print (help_msg)
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print (help_msg)
                sys.exit()
            elif opt in ("-i", "--db"):
                in_db = arg
            elif opt in ("-a", "--address"):
                address = arg        
            elif opt in ("--port", '-p'):
                port = int(arg)                        
            elif opt in ("--max_num_msg"):
                max_num_msg = int(arg) 
            elif opt in ("--hash"):
                hash_file = arg
            elif opt in ("--type"):
                read_type = arg
                assert read_type in ["pacbio", "illumina"]                                                              
    
    assert utils.file_exists(hash_file), hash_file
    hash_file = os.path.abspath(hash_file)    
    
    client = HashClient(hash_file, in_db, address, port, max_num_msg, read_type);
    try:
        logger.info("initializing ")
        client.init()
        logger.info("Start to run")
        client.run()
    except:
        raise
    finally:
        client.close()
        logger.info("cleaned up")


if __name__ == "__main__":
    main(sys.argv[1:])
