#!/usr/bin/env python

import os
import sys, getopt
import utils
import config
from joblib import Parallel, delayed
from tqdm import tqdm
import dask.dataframe as dd
import numpy as np 
from hashlib import sha256
from task import Task
    
logger = utils.get_logger("make_reads_hash")


def make_wc(in_folder, tmp_dir, n_thread):    
    if config.IS_BRIDGES:
        inbase = os.path.basename(in_folder);
        in_folder = os.path.join(tmp_dir, inbase);

    logger.info("make word count with " + str(locals()))

    files = utils.list_dir(in_folder, 'reads*.hashed*')
    assert len(files) > 0
    if 'DEBUG' in os.environ:
        files = files[:2]
    cmds = []
    for i, fpath in enumerate(files):
        if fpath.endswith('.gz'):
            cmd = "cat {} | gunzip "
        elif fpath.endswith('.zst'):
            cmd = "cat {} | zstd -d "
        else:
            cmd = "cat {}  "

        cmd += " | java -Xmx10G -cp {} WordCountOptimized > wc_{}.wordcount"

        cmd = cmd.format(fpath, os.path.join(os.environ['CAMI2_HOME'], 'lib', 'wc.jar'), i)
        cmds.append(cmd)
            
    assert len(cmds) > 0            
    status = Parallel(n_jobs=n_thread)(delayed(utils.shell_run_and_wait2)(cmd, tmp_dir) for cmd in tqdm(cmds))
    for i, u in enumerate(status):
        if u <> 0: 
            logger.error("Run command failed for: " + cmds[i])
            raise Exception("failed")

        
def merge_wc0(in_file, tmp_dir, kmer_out, label_out, n_therad):
    logger.info("merge wc " + str(locals()))
    os.chdir(tmp_dir)
    df = dd.read_csv("/wc_*.wordcount", header=None, sep="\t", dtype={0:np.str, 1:np.int})
    df.columns = ['word', 'count']
    df = df.groupby(['word']).sum().reset_index()
    df['islabel'] = df['word'].map(lambda u: u.upper().startswith('__LABEL__'))
    df = df.compute()
    logger.info("\n" + str(df.head()))
    
    df[df['islabel']][['word', 'count']].to_csv(label_out, header=None, index=None, sep=" ", compression='gzip' if label_out.endswith('.gz') else None)
    df[~df['islabel']][['word', 'count']].to_csv(kmer_out, header=None, index=None, sep=" ", compression='gzip' if kmer_out.endswith('.gz') else None)
    
    logger.info("finish merge wc.")    

    
def merge_wc(in_file,tmp_dir, kmer_out, label_out, n_thread):

    logger.info("merge wc " + str(locals()))
    os.chdir(tmp_dir)

    script = """
    
    val textFile = sc.textFile("wc_*.wordcount")
    val counts = textFile.map(line => line.split('\\t')).map(word => (word(0), word(1).toInt)).reduceByKey(_ + _).map(u=>u._1+'\\t'+u._2.toString)
    counts.saveAsTextFile("word.count")
    """
    
    mem = config.get_java_memory(n_thread)

    with open(os.path.join(tmp_dir, "word_count.scala"), 'wt') as fin:
        fin.write(script)
    
    cmd = """
     rm -fr word.count && {} --master local[{}] --conf spark.driver.memory={}G   < word_count.scala && cat word.count/part-* > word_count.txt """.format(
        os.path.join(os.environ['SPARK_HOME'], 'bin', "spark-shell"),
        n_thread, mem
        )

    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir)
    if (status != 0):
        raise Exception("word count failed")

    df = dd.read_csv("word_count.txt", header=None, sep="\t", dtype={0:np.str, 1:np.int})
    df.columns = ['word', 'count']
    df = df.groupby(['word']).sum().reset_index()
    df['islabel'] = df['word'].map(lambda u: u.upper().startswith('__LABEL__'))
    df = df.compute()
    logger.info("\n" + str(df.head()))
    
    df[df['islabel']][['word', 'count']].to_csv(label_out, header=None, index=None, sep=" ", compression='gzip' if label_out.endswith('.gz') else None)
    df[~df['islabel']][['word', 'count']].to_csv(kmer_out, header=None, index=None, sep=" ", compression='gzip' if kmer_out.endswith('.gz') else None)
    
    logger.info("finish merge wc.")    


def preparation(in_file, tmp_dir):
    utils.remove_if_file_exit(tmp_dir, is_dir=True)
    utils.create_dir_if_not_exists(tmp_dir)


def copy_input(in_file, tmp_dir):
    if not config.IS_BRIDGES: return in_file
    inbase = os.path.basename(in_file);
    stage = os.path.join(tmp_dir, inbase);    
    logger.info("Copying file from {} to {}".format(in_file, stage))
    
    cmd = """
    RC=1
    n=0
    while [[ $RC -ne 0 && $n -lt 20 ]]; do
        rsync -aP {sourcedir} {tmp_dir}
        RC=$?
        let "n = n + 1"
        sleep 10
    done

    """.format(sourcedir=in_file, tmp_dir=tmp_dir)
    
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir)
    if status != 0:
        logger.error("Run command failed for: " + cmd)
        raise Exception("failed")    

    return stage
        
        
def run(in_file, kmer_file, label_out , tmp_dir, n_thread):
    params = str(locals())
    logger.info("params: " + params)
    thisid = sha256(params).hexdigest();
    if tmp_dir is None: 
        tmp_dir = os.path.join(config.LOCAL_PATH, thisid)
    logger.info("use tmp dir: " + tmp_dir)
    logger.info("n_thread: " + str(n_thread))

    if 1:
        task = Task('kmer_count-preparation-{}'.format(thisid), lambda _: preparation(in_file, tmp_dir))
        task()
        task = Task('kmer_count-copy_input-{}'.format(thisid), lambda _: copy_input(in_file, tmp_dir))
        task()        
    if 1:
        task = Task('kmer_count-make_wc-{}'.format(thisid), lambda _: make_wc(in_file, tmp_dir, n_thread))
        task()
    if 1:
        task = Task('kmer_count-merge_wc-{}'.format(thisid), lambda _: merge_wc(in_file,tmp_dir, kmer_file, label_out, n_thread))
        task()

        
def main(argv):
    in_file = ''
    out_file = ''
    label_out = ""
    n_thread = utils.get_num_thread()
    tmp_dir = None
    help_msg = sys.argv[0] + ' -i <hashed file>  -o <kmer count output> -l <label count output> [--thread <num thread>  --tmpdir <tmp dir>]'
    if len(argv) < 2:
        print (help_msg)
        sys.exit(2) 
    else:
        try:
            opts, args = getopt.getopt(argv, "hi:o:l:", ["thread=", 'tmpdir='])
        except getopt.GetoptError:
            print (help_msg)
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print (help_msg)
                sys.exit()
            elif opt in ("-i", "--in_file"):
                in_file = arg
            elif opt in ("--thread"):
                n_thread = int(arg) 
                assert n_thread > 0
            elif opt in ("--tmpdir"):
                tmp_dir = arg
            elif opt in ("-l"):
                label_out = arg                                          
            elif opt in ("-o", "--out_file"):
                out_file = arg  

    assert "CAMI2_HOME" in os.environ
    assert utils.file_exists(in_file), in_file
    in_file = os.path.abspath(in_file)

    if not label_out:
        label_out = os.path.join(in_file, "label_count.txt.gz")
    if not out_file:
        out_file = os.path.join(in_file, "kmer_count.txt.gz")
                
    label_out = os.path.abspath(label_out)    
    out_file = os.path.abspath(out_file)
    if utils.file_exists(out_file) or utils.file_exists(label_out):
        raise Exception("Delete output first: " + out_file + " ," + label_out)
    
    run(in_file, out_file, label_out , tmp_dir, n_thread);


if __name__ == "__main__":
    main(sys.argv[1:])
