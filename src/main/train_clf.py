#!/usr/bin/env python

import os
from hashlib import sha256
import sys, getopt
import utils
import config
from joblib import Parallel, delayed
from tqdm import tqdm
from myzstd import ZstdFile
import data
import pandas as pd 
from task import Task
    
logger = utils.get_logger("train_clf")
            

def count_words(tmp_dir, n_thread, mincount=5):
    script = """
    
    val textFile = sc.textFile("reads_*.mapped")
    val counts = textFile.flatMap(line => line.split(" ")).map(word => (word, 1)).reduceByKey(_ + _).filter(_._2>={}).map(u=>u._1+" "+u._2.toString)
    counts.saveAsTextFile("word.count")
    
    """.format(mincount)
    if config.IS_BRIDGES:
        mem = n_thread * 30
    else:
        from psutil import virtual_memory
        mem = virtual_memory()
        mem = int(mem.total * 0.8 / 1024 ** 3)

    with open(os.path.join(tmp_dir, "word_count.scala"), 'wt') as fin:
        fin.write(script)
    
    cmd = "{} --master local[{}] --executor-memory {}G   < word_count.scala && cat word.count/part-* > word_count.txt ".format(
        os.path.join(os.environ['SPARK_HOME'], 'bin', "spark-shell"),
        n_thread, mem
        )

    shell_file = os.path.join(tmp_dir, 'word_count.sh')
    with open(shell_file, 'wt') as fin:
        fin.write(cmd)
    status = utils.shell_run_and_wait("bash word_count.sh", working_dir=tmp_dir)
    if (status != 0):
        raise Exception("word count failed")
    logger.info("finish word count.")   
    return os.path.join(tmp_dir, 'word_count.txt') 


def make_data(tmp_dir, tmp_data_file, n_thread):
    
    if 1:
        cmd = "cat reads_*.mapped >  {} "
        cmd = cmd.format(tmp_data_file)
    
    logger.info("Running: " + cmd)
    
    shell_file = os.path.join(tmp_dir, 'make_data.sh')
    with open(shell_file, 'wt') as fin:
        fin.write(cmd)
    status = utils.shell_run_and_wait("bash make_data.sh", working_dir=tmp_dir)
    if (status != 0):
        raise Exception("copy files failed")
    logger.info("finish copy hashed.")    


def map_a_file(fname, labeltype, tmp_dir):
    label_mapping = data.NCBITaxonomy()
    map_fun = label_mapping.make_map_fun(labeltype)
    start_lable_pos = len("__LABEL__")
    outputfilename = os.path.basename(fname) + ".mapped"
    outputfilename = os.path.join(tmp_dir, outputfilename)
    logger.info('mapping {} to {}'.format(fname, outputfilename))    
    with ZstdFile(fname) as fin, open(outputfilename, 'wt') as fout:
        for line in fin:
            pos = line.find(' ', 0)
            # 
            label = int(line[start_lable_pos:pos])
            label = map_fun(label)
            if label > 0:
                seq = line[pos:]
                label = "__label__" + str(label)
                newline = label + seq
                fout.write(newline)

            
def map_labels(in_folder, label, tmp_dir, n_thread):
    if config.IS_BRIDGES: 
        inbase = os.path.basename(in_folder);
        in_folder = os.path.join(tmp_dir, inbase);    
    
    logger.info("map_labels ...")
    files = utils.list_dir(in_folder, "reads_*.split.hashed.zst")
    if 'DEBUG' in os.environ:
        files = files[:2]
    assert len(files) > 0
    if n_thread <= 1:
        for fname in files:
            logger.info("mapping " + fname)
            map_a_file(fname, label, tmp_dir)
    else:
        Parallel(n_jobs=n_thread)(delayed(map_a_file)(u, label, tmp_dir) for u in tqdm(files))
    
    newfiles = utils.list_dir(tmp_dir, "reads_*.mapped")
    assert len(files) == len(newfiles)
    logger.info("finish map_labels ...")


def make_wc(labeltype, tmp_dir, in_folder, n_thread):
    if config.IS_BRIDGES: 
        inbase = os.path.basename(in_folder);
        in_folder = os.path.join(tmp_dir, inbase);    

    wc_file = os.path.join(tmp_dir, 'all_word_count.txt')

    wc2 = in_folder + "/label_count.txt.gz"
    df2 = pd.read_csv(wc2, header=None, sep=" ")
    logger.info("\n" + str(df2.head()))
    
    label_mapping = data.NCBITaxonomy()
    map_fun = label_mapping.make_map_fun(labeltype)
    start_lable_pos = len("__LABEL__")

    def f(u):
            label = int(u[start_lable_pos:])
            label = map_fun(label)
            if label > 0:
                label = "__label__" + str(label)
                return label
            else:
                return None

    df2[0] = df2[0].map(f)
    df2 = df2.dropna()

    wc1 = in_folder + "/kmer_count.txt.gz"
    df1 = pd.read_csv(wc1, header=None, sep=" ")
    logger.info("\n" + str(df1.head()))
    
    df = pd.concat([df2, df1], axis=0)
    logger.info("\n" + str(df.head()))
    df.to_csv(wc_file, sep=" ", header=None, index=None)
    
    return wc_file


def train(tmp_data_file, wc_file, outmodel, params, n_thread):
    logger.info("training model ... ")
    tmpoutmodel = os.path.join(config.MEMDISK_PATH, utils.unique_name())
    cmd = "fastseq supervised -input {} -output {} -thread {} -wcFile {} ".format(tmp_data_file, tmpoutmodel, n_thread, wc_file)
    cmd += " ".join('-{} {}'.format(u, v) for u, v in params.items())
    
    logger.info("Running: " + cmd)    
    status = utils.shell_run_and_wait(cmd)
    if status != 0:
        logger.info("run fastseq failed")
        raise Exception("run fastseq failed")
    else:
        logger.info("finish running train")
        
    cmd = "cp {}.bin {}".format(tmpoutmodel, outmodel)
    logger.info("Running: " + cmd)    
    status = utils.shell_run_and_wait(cmd)
    if status != 0:
        logger.info("copying model failed")
        raise Exception("copying model failed")
    else:
        logger.info("finish copying model")


def copy_input(in_file, tmp_dir):
    if not config.IS_BRIDGES: 
        return in_file
    inbase = os.path.basename(in_file);
    stage = os.path.join(tmp_dir, inbase);    
    logger.info("Copying file from {} to {}".format(in_file, stage))
    
    cmd = """
    RC=1
    n=0
    while [[ $RC -ne 0 && $n -lt 20 ]]; do
        rsync -aP {sourcedir} {tmp_dir}
        RC=$?
        let "n = n + 1"
        sleep 10
    done

    """.format(sourcedir=in_file, tmp_dir=tmp_dir)
    
    status = utils.shell_run_and_wait2(cmd, working_dir=tmp_dir, logger=logger)
    if status != 0:
        logger.error("Run command failed for: " + cmd)
        raise Exception("failed")    

    return stage


def preparation(in_file, tmp_dir):
    utils.remove_if_file_exit(tmp_dir, is_dir=True)
    utils.create_dir_if_not_exists(tmp_dir)

                
def run(in_file, label, out_file, tmp_dir, tmp_data_file, fastseq_args, n_thread):
    params=dict(locals())
    params['alg'] = 'fastseq'    
    params = str(locals())
    logger.info("params: " + params)
    thisid = sha256(params).hexdigest();
    if tmp_dir is None: 
        tmp_dir = os.path.join(config.LOCAL_PATH, thisid)
    if tmp_data_file is None:
        tmp_data_file = os.path.join(config.MEMDISK_PATH, thisid + ".data")
    logger.info("use tmp dir: " + tmp_dir)
    logger.info("n_thread: " + str(n_thread))

    if 1:
        task = Task('train_clf-preparation-{}'.format(thisid), lambda _: preparation(in_file, tmp_dir))
        task()
    if 1:
        task = Task('train_clf-copy_input-{}'.format(thisid), lambda _: copy_input(in_file, tmp_dir))
        task()
                
    if 1:

        def g():
            # wc_file = count_words(tmp_dir, n_thread)        
            global wc_file
            wc_file = make_wc(label, tmp_dir, in_file, n_thread)
            map_labels(in_file, label, tmp_dir, n_thread)

        task = Task('train_clf-make_labels-{}'.format(thisid), lambda _: g())
        task()            
    if 1:
        make_data(tmp_dir, tmp_data_file, n_thread)
    if 1:
        train(tmp_data_file, wc_file, out_file, fastseq_args, n_thread)

        
def main(argv):
    in_file = ''
    out_file = ''
    label = ""
    lr = 0.1
    ws = 5
    epoch = 5
    inputModel = ""
    bucket = 20000000
    dim = 100
    n_thread = utils.get_num_thread()
    tmp_dir = None
    tmp_data_file = None
    help_msg = sys.argv[0] + ' -i <hashed file folder>  -o <output model> --label <label type> [--lr <lr> --ws <ws> --epoch <epoch> --inputModel <input model> --bucket <bucket> --dim <dim> --thread <num thread>  --tmpdir <tmp dir>]'    
    if len(argv) < 2:
        print (help_msg)
        sys.exit(2) 
    else:
        try:
            opts, args = getopt.getopt(argv, "hi:o:", ["thread=", "label=", 'tmpdir=', 'hash=', 'lr=', 'ws=', 'epoch=', 'bucket=', 'dim=', 'inputModel='])
        except getopt.GetoptError:
            print (help_msg)
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print (help_msg)
                sys.exit()
            elif opt in ("-i", "--in_file"):
                in_file = arg
            elif opt in ("--thread"):
                n_thread = int(arg) 
                assert n_thread > 0
            elif opt in ("--tmpdir"):
                tmp_dir = arg
            elif opt in ("--label"):
                label = arg
                assert label in ['superkingdom', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']                
            elif opt in ("-o", "--out_file"):
                out_file = arg
            elif opt in ("--lr"):
                lr = float(arg)
                assert lr > 0 
            elif opt in ("--ws"):
                ws = int(arg)
                assert ws > 0
            elif opt in ("--epoch"):
                epoch = int(arg)
                assert epoch > 0
            elif opt in ("--bucket"):
                bucket = int(arg)
                assert bucket > 0
            elif opt in ("--dim"):
                dim = int(arg)
                assert dim > 0
            elif opt in ("--inputModel"):
                inputModel = (arg)

    excepttext = []
    
    if False and "SPARK_HOME" not in os.environ:
        excepttext.append("must specify SPARK_HOME env")
    
    if inputModel and not utils.file_exists(inputModel):
        excepttext.append(inputModel + " not exists")     
        inputModel = os.path.abspath(inputModel)
    
    if not utils.file_exists(in_file):
        excepttext.append(in_file + " not exists")
    in_file = os.path.abspath(in_file)
    
    if not utils.file_exists(in_file + "/kmer_count.txt.gz"):
        excepttext.append(in_file + "/kmer_count.txt.gz" + " not exists")
        
    if not utils.file_exists(in_file + "/label_count.txt.gz"):
        excepttext.append(in_file + "/label_count.txt.gz" + " not exists")        
    
    if utils.file_exists(out_file + ".model"):
        excepttext.append("delete output model first: " + out_file)
    
    if excepttext:
        raise Exception(str(excepttext))
    
    fastseq_args = dict(lr=lr, ws=ws, epoch=epoch, bucket=bucket, dim=dim)
    if inputModel: 
        fastseq_args['inputModel'] = inputModel
        fastseq_args['incr'] = 1
    if label:
        if label == 'species':
            loss = 'hs'
        else:
            loss = 'softmax'
    else:
        raise Exception("NA")
    fastseq_args['loss'] = loss
    run(in_file, label, out_file, tmp_dir, tmp_data_file, fastseq_args, n_thread);


if __name__ == "__main__":
    main(sys.argv[1:])
