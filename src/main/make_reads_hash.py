#!/usr/bin/env python

import os
import gzip
import sys, getopt
import random
import re
import utils
import config
import randproj
import csparc
from joblib import Parallel, delayed
from tqdm import tqdm
from myzstd import ZstdFile
    
logger = utils.get_logger("make_reads_hash")


def split_input(fpath, tmpdir, splitsize):
    logger.info("split input with " + str(locals()))
    if fpath.endswith('.gz'):
        cmd = "cat {} | gunzip "
    elif fpath.endswith('.zst'):
        cmd = "cat {} | zstd -d "
    else:
        cmd = "cat {}  "
    
    cmd += " | split --additional-suffix .split -C {} - reads_  "
    
    cmd = cmd.format(fpath, splitsize.upper())
    
    logger.info("Running: " + cmd)
    
    shell_file = os.path.join(tmpdir, 'split_input.sh')
    with open(shell_file, 'wt') as fin:
        fin.write(cmd)
    status = utils.shell_run_and_wait("bash split_input.sh", working_dir=tmpdir)
    if (status != 0):
        raise Exception("split input files failed")
    logger.info("finish splitting.")


def hash_a_file(fname, hashfile):
    status = randproj.load_from_file(hashfile)
    assert status == 0, 'load hash failed for ' + hashfile
    kmer_size = randproj.get_kmer_size()
    outputfilename = fname + ".hashed.zst"
    logger.info('hashing {} to {}, k={}'.format(fname, outputfilename, kmer_size))    
    with open(fname) as fin, ZstdFile(outputfilename, 'w') as fout:
        for line in fin:
            _, taxid_rownum, seq = line.strip().split("\t")
            taxid, rownum = taxid_rownum.split("-")
            kmers = csparc.generate_kmer_for_fastseq(seq, kmer_size)
            hashed = [randproj.hash_kmer(kmer, reverse_compliments=True) for kmer in kmers]
            hashed = [csparc.ulong2base64(i) for i in hashed]
            newline = "__label__{} ".format(taxid) + " ".join(hashed) + "\n"
            fout.write(newline)
            

def copy_hashed(tmpdir, outputfilepath):
    cmd = "cp reads_*.split.hashed.zst {} "
    cmd = cmd.format(outputfilepath)
    
    logger.info("Running: " + cmd)
    
    shell_file = os.path.join(tmpdir, 'copy_hashed.sh')
    with open(shell_file, 'wt') as fin:
        fin.write(cmd)
    status = utils.shell_run_and_wait("bash copy_hashed.sh", working_dir=tmpdir)
    if (status != 0):
        raise Exception("copy files failed")
    logger.info("finish copy hashed.")    


def make_hash(tmp_dir, hashfile, n_thread):
    logger.info("make hash ...")
    files = utils.list_dir(tmp_dir, "reads_*.split")
    assert len(files) > 0
    if n_thread <= 1:
        for fname in files:
            logger.info("hashing " + fname)
            hash_a_file(fname, hashfile)
    else:
        Parallel(n_jobs=n_thread)(delayed(hash_a_file)(u, hashfile) for u in tqdm(files))
    
    newfiles = utils.list_dir(tmp_dir, "reads_*.split.hashed.zst")
    assert len(files) == len(newfiles)
    logger.info("finish hashing ...")
            
        
def main(argv):
    in_file = ''
    out_file = ''
    hash_file = ""
    split_size = "1G"
    n_thread = utils.get_num_thread()
    tmp_dir = os.path.join(config.MEMDISK_PATH, utils.unique_name())
    help_msg = sys.argv[0] + ' -i <read seq_file>  -o <output> --hash <hash file> [--thread <num thread> --split <split_size> --tmpdir <tmp dir>]'
    if len(argv) < 2:
        print (help_msg)
        sys.exit(2) 
    else:
        try:
            opts, args = getopt.getopt(argv, "hi:o:", ["thread=", "split=", 'tmpdir=', 'hash='])
        except getopt.GetoptError:
            print (help_msg)
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print (help_msg)
                sys.exit()
            elif opt in ("-i", "--in_file"):
                in_file = arg
            elif opt in ("--thread"):
                n_thread = int(arg) 
                assert n_thread > 0
            elif opt in ("--split"):
                split_size = arg
            elif opt in ("--tmpdir"):
                tmp_dir = arg
            elif opt in ("--hash"):
                hash_file = arg                                          
            elif opt in ("-o", "--out_file"):
                out_file = arg  

    assert utils.file_exists(in_file), in_file
    in_file = os.path.abspath(in_file)
    
    assert utils.file_exists(hash_file), hash_file
    hash_file = os.path.abspath(hash_file)    
    
    out_file = os.path.abspath(out_file)
    if utils.file_exists(out_file):
        raise Exception("Delete output first: "+out_file)
    utils.create_dir_if_not_exists(out_file)
    
    run(hash_file, in_file, out_file, tmp_dir, split_size, n_thread);


def run(hash_file, in_file, out_file, tmp_dir, split_size, n_thread):
    logger.info("params: " + str(locals()))
    logger.info("use tmp dir: " + tmp_dir)
    logger.info("lsh file: " + hash_file)
    logger.info("n_thread: " + str(n_thread))

    if 1:
        utils.remove_if_file_exit(tmp_dir, is_dir=True)
        utils.create_dir_if_not_exists(tmp_dir)
        split_input(in_file, tmp_dir, split_size)
    if 1:
        make_hash(tmp_dir, hash_file, n_thread)
    if 1:
        copy_hashed(tmp_dir, out_file)


if __name__ == "__main__":
    main(sys.argv[1:])
