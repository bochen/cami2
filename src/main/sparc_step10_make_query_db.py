import fastparquet 
import numpy as np
import config, utils
from task import Task
import os
from joblib import Parallel, delayed
import cPickle as pickle
import re
from itertools import izip_longest
from tqdm import tqdm
import randproj
import csparc as kg 


def crp_hash_read(read, kmer_size):
    kmers = kg.generate_kmer_for_fastseq(read, kmer_size)
    if len(kmers) > 0:
        return [randproj.hash_kmer(u, True) for u in kmers]
    else:
        return None


def onehot_hash_read(read, kmer_size):
    kmers = kg.generate_kmer_number(read, kmer_size)
    if len(kmers) > 0:
        return kmers
    else:
        return None


def py_hash_read(read):
    global rp
    ret = rp.hash_read(read)
    if ret is None:
        return None
    else:
        return ret[0]


def process_one_hash(abc):
    idx, tax, seq = abc
    id = "{}_{}".format(tax, idx)
    global conn, rp_fun
    ret = rp_fun(seq)
    if ret is None:
        pass
    else:
        if 1:
            pipe = conn.pipeline(transaction=False)
            for u in ret:
                pipe.sadd(u, id)
            pipe.execute()
        else:
            for u in ret:
                conn.sadd(u, id)


def sparc_make_ref(name, sample_length, kmer_size, hash_size, coverage=5, batch_size=1000 * 1000, rpname="py"):

    def get_rp():
        if rpname == 'py':
            fname = os.path.join(config.LSH_PATH, 'lsh_nt_NonEukaryota_k{}_h{}.pkl'.format(kmer_size, hash_size))
            print ("load from " + fname)
            with open(fname) as fin:
                return pickle.load(fin)
        elif rpname == 'crp':
            fname = os.path.join(config.LSH_PATH, 'lsh_nt_NonEukaryota_k{}_h{}.crp'.format(kmer_size, hash_size))
            print ("load from " + fname)
            randproj.load_from_file(fname)

    def get_candidates():
        df2 = fastparquet.ParquetFile(os.path.join(config.INPUT_PATH, 'nt_info_NonEukaryota.parq')).to_pandas()
        df3 = df2[df2['superkingdom'].isin({'Bacteria', 'Viruses', 'Archaea', 'Viroids'})]
        
        nas = set(['NA', 'NA2'])
        df10 = df3[(~df3['phylum'].isin(nas))]
        total_len = df10['length'].sum()        
        df10 = df10.set_index('row')['taxid']
        return df10.to_dict(), total_len
    
    def get_redis():
        import redis
        # redis-server --port 16379 --dir /mnt/redis_data
        r = redis.StrictRedis(host='localhost', port=16379, db=0)
        return r 

    def clear_redis():
        conn.flushdb()

    # iterate a list in batches of size n
    def batcher(iterable, n):
        args = [iter(iterable)] * n
        return izip_longest(*args)
    
    def process_batch(lst):
        if 1:
            # Parallel(n_jobs=utils.get_num_thread())(delayed(process_one_hash)(abc) for abc in tqdm(lst))
            utils.parallell_for(lst, process_one_hash, n_jobs=utils.get_num_thread())
        else:
            for u in tqdm(lst):
                process_one_hash(u)

    redis_name = 'ardb'
    sample_name = 'nt_NonEukaryota'
    dbfilename = "{}_l{}_k{}_h{}_c{}.rdb".format(sample_name, sample_length, kmer_size, hash_size, coverage)
    sub_re = re.compile(r"[^ACGT]")
    global conn, rp, rp_fun
    conn = get_redis()
    if redis_name == 'redis':
        conn.config_set('dbfilename', dbfilename)
    
    rp = get_rp()        
    if rpname == 'py':    
        rp_fun = py_hash_read
    elif rpname == 'crp':    
        rp_fun = lambda u: crp_hash_read(u, kmer_size)
    elif rpname == 'onehot':
        rp_fun = lambda u: onehot_hash_read(u, kmer_size)
    else:
        raise Exception("unknown " + rpname)
        
    def f(logger):
        clear_redis()
        logger.info("cleared redis")

        candidates, total_len = get_candidates()
        logger.info("will load %d references", len(candidates))
        logger.info("about %d reads will be processed", int(1.0 * total_len / sample_length * coverage))

        reads = []
        db = utils.create_db(os.path.join(config.INPUT_PATH, 'nt_split_NonEukaryota.db'))
        
        idx = 0
        it = db.iteritems()
        it.seek_to_first()
        n_keys = 0
        for key, val in it:
            n_keys += 1
            if (n_keys % 10000 == 0):
                logger.info("db iteration: " + str(n_keys))
            key = int(key)
            if key in candidates:
                if len(val) > sample_length * 2:                
                    val = sub_re.sub("N", val.strip())
                    num = int(1.0 * len(val) / sample_length * coverage)
                    for _ in range(num):
                        i = int(np.random.random() * (len(val) - sample_length))
                        reads.append((idx, candidates[key], val[i:i + sample_length]))
                        idx += 1
                        if len(reads) >= batch_size:
                            process_batch(reads)
                            logger.info("processed %d reads, total %d", len(reads), idx)                            
                            reads = []
            
            # if idx > 1000: break  # debug
            
        if len(reads) > 0:
            process_batch(reads)
            reads = []                            
            logger.info("processed %d reads", idx)

        if 1:
            conn.save()
            logger.info("saved " + dbfilename)
        
        if redis_name == 'redis':
            outpath = os.path.join(config.SPARC_PATH, "{}_l{}_k{}_h{}_c{}.kref".format(sample_name, sample_length, kmer_size, hash_size, coverage))
            with open(outpath, 'wt') as fout:
                for keybatch in batcher(conn.scan_iter('*'), 500):
                    keybatch = [u for u in keybatch if u is not None]
                    for key in keybatch:
                        values = conn.smembers(key)
                        fout.write("{}\t{}\n".format(key, " ".join(values)))
                    
    task = Task('sparc_make_ref-{}_l{}_k{}_h{}_c{}_{}'.format(sample_name, sample_length, kmer_size, hash_size, coverage, rpname), lambda u: f(u))
    task()

               
def run():
    if 1:
        sparc_make_ref("NonEukaryota", sample_length=150 * 2, kmer_size=31, hash_size=25, coverage=0.1, batch_size=1000 * 100, rpname='onehot')


if __name__ == '__main__':
        run()

