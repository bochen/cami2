import fastparquet 
import pandas as pd 
import numpy as np
import config, utils
from task import Task
import os
import gzip
from joblib import Parallel, delayed
import re

def splt_nr():

    def process_fasta(lines):
        ret = []
        head = None;      seq = ""
        for line in lines:
            if line.startswith(">"):
                if head is not None and seq:
                    ret.append((head, seq.upper()))
                    head = None; seq = ""
                head = line
            else:
                if head is not None:
                    seq += line
        if head is not None and seq:
            ret.append((head, seq.upper()))                    
        return ret
                    
    def f(logger):
        fpath = config.NT_ZIP
        assert utils.file_exists(fpath), fpath
        
        total_count = 0
        fileheader = os.path.join(config.INPUT_PATH, "nt_head.txt")
        fileseq = os.path.join(config.INPUT_PATH, "nt_seq.txt")
        batchsize = 1000000
        lines = []
        with gzip.open(fpath, 'rt') as fin, open(fileheader, 'wt') as fh, open(fileseq, 'wt') as fs:
            for line in fin:
                line = line.strip()
                if len(lines) >= batchsize and line.startswith(">"):
                    ret = process_fasta(lines)
                    for head, seq in ret:
                        aid = head.split(" ")[0][1:]
                        fh.write("{}\t{}\t{}\n".format(aid, len(seq), head))
                        fs.write(seq + "\n")
                    total_count += len(ret)
                    logger.info("processed {} reads".format(total_count))    
                    lines = []
                lines.append(line)                    

            if len(lines) > 0:
                ret = process_fasta(lines)
                for head, seq in ret:
                    aid = head.split(" ")[0][1:]
                    fh.write("{}\t{}\t{}\n".format(aid, len(seq), head))
                    fs.write(seq + "\n")
                total_count += len(ret)
                logger.info("processed {} reads".format(total_count))
                lines = []                     
        
    task = Task('splt_nr', lambda u: f(u))
    task()
 

def sample_reads(short_seq):
    sample_rate = 0.001
    sample_name = 'illumina' if short_seq else 'pacbio'
    
    def process_one(fname):
        print ("processing " + fname)
        ret = []
        
        with gzip.open(fname, 'rt') as fin:
            for line in fin:
                if np.random.random() < sample_rate:
                    ret.append(line)
        return ret
                    
    def f(logger):
        logger.info("list fold: " + config.SEQ_PATH)
        if not short_seq:
            logger.info("find long seqs ")
            files = utils.list_dir(config.SEQ_PATH, 'strmgCAMI2_long*.seq.gz') + utils.list_dir(config.SEQ_PATH, 'marmgCAMI2_long*.seq.gz')
        else:
            logger.info("find short seqs")
            files = utils.list_dir(config.SEQ_PATH, 'strmgCAMI2_short*.seq.gz') + utils.list_dir(config.SEQ_PATH, 'marmgCAMI2_short*.seq.gz')
            
        logger.info("Total %d files", len(files))
        logger.info(str(files))  
        assert len(files) == 110

        # files = files[:2]

        lst = Parallel(n_jobs=utils.get_num_thread())(delayed(process_one)(fname) for fname in files)
        
        outfilename = os.path.join(config.INPUT_PATH, "CAMI2_" + sample_name + "_sample.seq.gz")
        logger.info("Write " + outfilename)
        with gzip.open (outfilename, 'wt') as fout:
            for sublist in lst:
                for line in sublist:
                    fout.write(line)

    task = Task('sample_reads-{}'.format(sample_name), lambda u: f(u))
    task()


def sample_nt_NonEukaryota():
    sample_rate = 0.01
    sample_name = 'nt_NonEukaryota'
    sub_re = re.compile(r"[^ACGT]")
    def f(logger):
        
        df2 = fastparquet.ParquetFile(os.path.join(config.INPUT_PATH, 'nt_info_NonEukaryota.parq')).to_pandas()
        df3 = df2[df2['superkingdom'].isin({'Bacteria', 'Viruses', 'Archaea', 'Viroids'})]

        nas = set(['NA', 'NA2'])
        df10 = df3[(~df3['phylum'].isin(nas)) & 
           (~df3['class'].isin(nas)) & 
           (~df3['order'].isin(nas)) & 
           (~df3['family'].isin(nas)) & 
           (~df3['genus'].isin(nas)) & 
           (~df3['species'].isin(nas))]        
        
        db = utils.create_db(os.path.join(config.INPUT_PATH, 'nt_split_NonEukaryota.db'))
        
        outfilename = os.path.join(config.INPUT_PATH, sample_name + "_sample.seq.gz")
        rows = np.random.permutation(df10['row'].values)
        N = int(len(rows) * sample_rate)
        logger.info("Write " + outfilename)
        with gzip.open (outfilename, 'wt') as fout:
            for i, rownum in enumerate(rows[:N]):
                seq = db.get(str(rownum)).strip()
                seq = sub_re.sub("N", seq)
                line = '{}\trow_{}\t{}\n'.format(i, rownum, seq)
                fout.write(line)
        
    task = Task('sample_reads-{}'.format(sample_name), lambda u: f(u))
    task()

               
def run():
    if 0:
        splt_nr()
        sample_reads(short_seq=False)
        sample_reads(short_seq=True)
        sample_nt_NonEukaryota()


if __name__ == '__main__':
        run()

