'''
Created on Feb 20, 2019

@author: bo
'''
import zstandard as zstd


class ZstdFile(object):
    
    def __init__(self, filepath, option='r', level=3):
        assert option in ['r', 'w'], 'only "r" or "w" can be set'
        self.filepath = filepath
        self.option = option
        self.level = level
        self.BUFFER_SIZE = 1024 * 1024
        self.readonly = option == 'r'

    def next(self):
        return self.__next__()
    
    def __next__(self):
        while True:
            idx = self.chunk.find('\n', 0)
            if idx >= 0:
                break
            else:
                this_chunk = self.reader.read(self.BUFFER_SIZE)
                if this_chunk:
                    self.chunk += this_chunk
                else:
                    break
        if idx < 0:
            raise StopIteration()
        else:
            line = self.chunk[:idx + 1]
            self.chunk = self.chunk[idx + 1:]
            return line
         
    def __iter__(self):
        assert self.readonly, 'writing file cannot be read'
        return self        
    
    def write(self, text):
        assert not self.readonly, 'readonly file cannot be written'
        self.compressor.write(text)
    
    def writelines(self,lines):
        assert not self.readonly, 'readonly file cannot be written'
        for line in lines:
            self.compressor.write(line)
            
    def __enter__(self):
        if self.option == 'r':
            self.fh = open(self.filepath, 'rb')
            self.dctx = zstd.ZstdDecompressor()
            self.reader = self.dctx.stream_reader(self.fh)            
            self.chunk = ""
        elif self.option == 'w':
            self.fh = open(self.filepath, 'wb')
            self.cctx = zstd.ZstdCompressor(level=self.level)
            self.compressor = self.cctx.stream_writer(self.fh)
            self.compressor.__enter__()
        return self

    def close(self):
        self.__exit__(None,None,None)
        
    def __exit__(self, type, value, traceback):
        if self.option == 'r':
            self.reader.close()
            self.fh.close()            
        elif self.option == 'w':
            self.compressor.flush()
            self.compressor.__exit__(type, value, traceback)
            self.fh.close()
            
            
            
