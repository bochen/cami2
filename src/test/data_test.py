'''
Created on Feb 7, 2019

@author: bo
'''
import unittest

import numpy as np 
from data import QuakeSeg, TrainTest


class Test(unittest.TestCase):

    def testQuakeSeg(self):
        seg = QuakeSeg(signal=np.array(range(10)), failtime=10, name='testQuakeSeg')
        seg.save("/tmp/testQuakeSeg.npz")
        seg2 = QuakeSeg.load("/tmp/testQuakeSeg.npz")
        print seg2.signal 
        print seg2.name 
        print seg2.failtime

    def testTrainTest(self):
        funs = [np.mean, np.std, lambda u: np.sum([u[:10]])]
        strategy = {'type':'rand', "test_ratio":0.2}
        data = TrainTest("train_seq_offset_112500", "", strategy=strategy)
        data.make_featuers(funs)
        data.save("/tmp/testTrainTest.npz")
        print [u.shape for u in TrainTest.load("/tmp/testTrainTest.npz")]

    def testTrainTest2(self):
        funs = [np.mean, np.std, lambda u: np.sum([u[:10]])]
        strategy = {'type':'quake', "test_quakes":[2, 3, 4]}
        data = TrainTest("train_seq_offset_112500", "", strategy=strategy)
        data.make_featuers(funs)
        data.save("/tmp/testTrainTest2.npz")
        print [u.shape for u in TrainTest.load("/tmp/testTrainTest2.npz")]

        
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testQuakeSeg']
    unittest.main()
