'''
Created on Mar 4, 2019

@author: bo
'''
import unittest


class Test(unittest.TestCase):


    def testName(self):
        import google.protobuf as a 
        print a.__version__
        from read_pb2 import Read
        read= Read()
        read.label=1
        read.kmer.extend([1,2,3])
        f = open("test_proto_msg_1.bin", "wb")
        f.write(read.SerializeToString())
        f.close()
        
        read= Read()
        f = open("test_proto_msg_0.bin", "wb")
        f.write(read.SerializeToString())
        f.close()        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()