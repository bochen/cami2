'''
Created on Feb 3, 2019

@author: bo
'''
import unittest
import numpy as np 

class Test(unittest.TestCase):


    def setUp(self):
        n = 1000
        limit_low = 0
        limit_high = 0.48
        my_data = np.random.normal(0, 0.5, n) \
                  + np.abs(np.random.normal(0, 2, n) \
                           * np.sin(np.linspace(0, 3*np.pi, n)) ) \
                  + np.sin(np.linspace(0, 5*np.pi, n))**2 \
                  + np.sin(np.linspace(1, 6*np.pi, n))**2
        
        scaling = (limit_high - limit_low) / (max(my_data) - min(my_data))
        my_data = my_data * scaling
        my_data = my_data + (limit_low - min(my_data))
        self.signal=my_data 
        
    def tearDown(self):
        pass


    def testName(self):
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()